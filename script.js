let userinput = document.getElementById('userinput');

function addUserInput(num){
    userinput.value += num;
}

function deleteNum(){
    userinput.value=userinput.value.slice(0,-1);
}

function clearScreen() {
    userinput.value = "";
}

function calculate() {
    userinput.value = eval(userinput.value);
}